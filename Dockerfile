FROM openjdk:8-jre-alpine
COPY target/Api-Investimentos-*.jar app.jar
CMD ["java", "-jar", "app.jar"]